package part;

/**
 * 2018-05-09
 * @author 윤승환
 *
 */
public class PartTwo extends AbsPart {

	private int a = 10;
	private int b = 20;
	private static int c = 20;
	
	@Override
	public void run() {
		
		int d = 0;
		if (c > 0) {
			d = a + b;
		}
		
		test(d);
		
	}
	
	private void test(int d) {
		c = c + d;
		String s = "Result > ";
		s += c;
		
		System.out.println(s);
	}
	
}
